package lifegame;

import entity.Board;

public class LifeGame {
	
	private Board p2;
	private Board p;
	
	
    public LifeGame(){
        this.p2 = new Board(20);
        this.p = new Board(20);
        p.content[17][17] = true;
        p.content[18][17] = true;
        p.content[17][18] = true;
        p.content[19][18] = true;
        p.content[17][19] = true;
    }
    
    public void run() {
    	while(!p.eq(p2)){
            UserInterface.displayGameBoard(p);
            try {
                Thread.sleep(100);
            } catch (InterruptedException e) {
                e.printStackTrace();
            }
            p2 =  p.clone();
            p = new Board(20);
            for (int x=0; x<20; x++){
                for (int y=0; y<20; y++) {
                    int nb = 0;
                    for (int v= -1; v <= 1; v++){
                        for (int h = -1; h <= 1; h++){
                            if (!(v== 0 && h==0)){
                                if (x+h >= 0 && x+h < 20 && y + v >= 0 && y+v < 20){
                                    boolean ok =  p2.content[x+h][y+v];
                                    if (ok){
                                        nb ++;
                                    }
                                }
                            }
                        }
                    }
                    if ( p2.content[x][y]){
                        if (nb < 2 || nb > 3){
                             p.content[x][y] = false;
                        } else {
                             p.content[x][y] = true;
                        }
                    } else {
                        if (nb == 3){
                             p.content[x][y] = true;
                        } else {
                             p.content[x][y] = false;
                        }
                    }
                }
            }
        }
        UserInterface.displayEndMessage();
    }
}