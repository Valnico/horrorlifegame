package entity;

public class Board {

    public boolean[][] content;

    public Board(int boardSize){
        this.content = new boolean[boardSize][boardSize];
        boolean[] l;
        for (int x=0; x<boardSize; x++){
            l = new boolean[boardSize];
            for (int y=0; y<boardSize; y++) {
                l[y] = false;
            }
            this.content[x] = l;
        }
    }

    public boolean eq(Board board2) {
        for (int x=0; x<this.content.length; x++){
            for (int y=0; y<this.content.length; y++) {
                if (this.content[x][y] != board2.content[x][y]){
                    return false;
                }
            }
        }
        return true;
    }

    public Board clone(){
        Board clone = new Board(this.content.length);
        for (int abscissa=0; abscissa<this.content.length; abscissa++){
            for (int y=0; y<this.content.length; y++) {
                clone.content[abscissa][y] = this.content[abscissa][y];
            }
        }
        return clone;
    }
}